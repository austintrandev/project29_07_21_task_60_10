package com.example.demo;

public class Fish extends Animals  {
	public Fish(int age, String gender, int size, boolean canEat) {
	
		this.age = age;
		this.gender = gender;
		this.size = size;
		this.canEat = canEat;
	}

	public Fish() {
		this.age = 1;
		this.gender = "male";
		this.size = 2;
		this.canEat = true;
	}
	
	private int size;
	private boolean canEat;
	@Override
	public boolean isMammal() {
		return false;
	}
	@Override
	public void mate() {
		System.out.println("Fish mate");
	}

	public void siwm() {
		System.out.println("Fish swimming");
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public boolean isCanEat() {
		return canEat;
	}

	public void setCanEat(boolean canEat) {
		this.canEat = canEat;
	}

}

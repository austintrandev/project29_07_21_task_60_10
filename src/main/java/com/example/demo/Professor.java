package com.example.demo;

import java.util.ArrayList;

public class Professor extends Person implements ISchool {
	public Professor(int age, String gender, String name, Address address, int salary) {

		this.salary = salary;
	}

	public Professor(int age, String gender, String name, Address address, int salary,ArrayList<Animals>listPet) {
		super(age, gender, name, address, listPet);
		this.salary = salary;
	}

	public Professor() {
		this.salary = 20000000;
	}

	private int salary;

	public void teaching() {
		System.out.println("Professor is teaching");
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	@Override
	public void eat() {
		// TODO Auto-generated method stub
		System.out.println("Professor eats hambuger");
	}

}

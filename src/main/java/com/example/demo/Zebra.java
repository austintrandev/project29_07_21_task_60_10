package com.example.demo;

public class Zebra extends Animals {
	public Zebra(int age, String gender, boolean is_wild) {
		super();
		this.age = age;
		this.gender = gender;
		this.is_wild = is_wild;
	}

	public Zebra() {
		super();
		this.age = 2;
		this.gender = "male";
		this.is_wild = true;
	}

	private boolean is_wild;
	@Override
	public boolean isMammal() {
		return true;
	}
	@Override
	public void mate() {
		System.out.println("Zebra mate");
	}

	public void run() {
		System.out.println("Zebra running");
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public boolean isIs_wild() {
		return is_wild;
	}

	public void setIs_wild(boolean is_wild) {
		this.is_wild = is_wild;
	}
}

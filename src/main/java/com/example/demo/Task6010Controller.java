package com.example.demo;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Task6010Controller {
	int type = 0;
	@CrossOrigin
	@GetMapping("/listPerson")
	public ArrayList<Person> getListPerson(@RequestParam(value = "type", defaultValue = "0") int type) {
		ArrayList<Person> listPerson = new ArrayList<Person>();
		
		ArrayList<Animals> listPet = new ArrayList<Animals>();
		Duck myDuck = new Duck();
		Fish myFish = new Fish(3, "female", 8, true);
		Zebra myZebra = new Zebra();
		listPet.add(myDuck);
		listPet.add(myZebra);
		listPet.add(myFish);

		Address professorAddress = new Address("Nguyen Hue", "HCM city", "Viet Nam", 1234);
		Professor myProfessor = new Professor(55, "male", "Peter", professorAddress, 60000000, listPet);
		Subject mySubject = new Subject("Physic", 101, myProfessor);
		Student myStudent = new Student(20, "female", "Mary", new Address("Nguyen Trai", "HCM City", "Viet Nam", 1234), 12,
				new ArrayList<Subject>() {
					{
						add(mySubject);
					}
				}, new ArrayList<Animals>() {
					{
						add(myZebra);
					}
				});

		Worker myWorker = new Worker(35, "male", "Kabuto", new Address("Pham Van Dong", "HCM city", "Viet Nam", 1234),
				25000000, new ArrayList<Animals>() {
					{
						add(myDuck);
					}
				});

		if (type == 1) {
			listPerson.add(myStudent);
		} else if (type == 2) {
			listPerson.add(myWorker);
		} else if (type == 3) {
			listPerson.add(myProfessor);
		} else {
			listPerson.add(myStudent);
			listPerson.add(myWorker);
			listPerson.add(myProfessor);
		}
		return listPerson;
	}
}
